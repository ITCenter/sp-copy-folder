#!/usr/bin/env node
const { sp } = require('@pnp/sp');
const spauth = require('pnp-auth');
const AuthConfig = require('node-sp-auth-config').AuthConfig;
const fs  = require('fs-extra');
const path  = require('path');
const winston  = require('winston');
const commandLineArgs  = require('command-line-args');
const commandLineUsage  = require('command-line-usage');

const options = require('./options.js');

const helpOptions = require('./helpOptions.js');

async function readdirRec(dir) {
    const dirents = await fs.readdir(dir);
    const files = await Promise.all(dirents.map((dirent) => {
        const res = path.resolve(dir, dirent);
        return fs.lstatSync(res).isDirectory() ? readdirRec(res) : res;
    }));
    return Array.prototype.concat(...files);
}

async function main(){
    const args = commandLineArgs(options,{ partial: true });
    if(args._unknown){
        console.log(`Unknown option ${args._unknown[0]}. Use "-h" or "--help" for help.`);
        return;
    }else if(args.help){
        const usage = commandLineUsage(helpOptions)
        console.log(usage)
        return;
    }

    const logger = winston.createLogger({
        level: args.verbose ? 'verbose' : 'info',
        format: winston.format.combine(
            winston.format.colorize(),
            winston.format.simple()
        ),
        transports: [new winston.transports.Console()]
    });

    const packageInfoFile = path.resolve(args.package);
    if(!fs.exists(packageInfoFile)){
        logger.error(`cannot find package file ${packageInfoFile}`);
        logger.error(`stopping deployment.`); 
    }
    const packageInfo = require(packageInfoFile);
    const packageFolderName = `${packageInfo.name}@${packageInfo.version}`;

    const spConfigFile = path.resolve(args.config);
    const spConfigLocalFile = path.join(path.dirname(spConfigFile), path.basename(spConfigFile, path.extname(spConfigFile)) + ".local.json");    

    logger.info(`starting deployment of ${packageFolderName}`);

    if(!await fs.exists(spConfigFile)){
        logger.warn(`cannot find SharePoint config file ${spConfigFile}`);
    }

    if(args.acceptJsonHeader){
        logger.verbose(`adding header "Accept: application/json;odata=verbose" to SharePoint API requests`);
        sp.setup({
            sp: {
                headers: {
                    Accept: "application/json;odata=verbose",
                }
            }
        });
    }

    const spAuth =  new AuthConfig({
        encryptPassword: true,
        saveConfigOnDisk: true,
        defaultConfigPath: spConfigFile,    
        configPath: spConfigLocalFile 
    });
    spauth.bootstrap(sp, spAuth);
    
    const spDocLibName = args.library;    
    const appDistDir = path.resolve(args.src);

    const { SiteFullUrl : spSiteUrl } = await sp.site.getContextInfo();
    const { LoginName : spSiteUser } = await sp.web.currentUser.get();

    logger.info(`will deploy to ${spDocLibName} on site ${spSiteUrl} using ${spSiteUser}`);

    let spDocLib;
    try{
        const { list, created } = await sp.web.lists.ensure(spDocLibName, "", 101, false, { Hidden: true });
        if(created){
            logger.verbose(`created new document library ${spDocLibName}`);
            await list.breakRoleInheritance(false);
            const { Id: roleDefId } = await sp.web.roleDefinitions.getByName('Read').get();
            const { Id: everyoneId } = await sp.web.siteUsers.getByLoginName('c:0(.s|true').get();
            await list.roleAssignments.add(everyoneId, roleDefId);
        }
        spDocLib = list;
    }catch(e){
        logger.error(`could not find or create document library ${spDocLibName} on site ${spSiteUrl}`);
        logger.error(`stopping deployment.`);
        return;
    }

    const distFiles = await readdirRec(appDistDir);
    logger.info(`will deploy ${distFiles.length} files from ${appDistDir}`);

    try{
        let existingFolder = spDocLib.rootFolder.folders.getByName(packageFolderName);        
        logger.warn(`will overwrite existing files of ${packageFolderName}`);
        await existingFolder.delete();
    }catch(e){}

    const { folder: packageRootFolder } = await spDocLib.rootFolder.folders.add(packageFolderName, true)

    for(fileName of distFiles){
        logger.verbose(`now processing ${fileName}`);
        const file = await fs.readFile(fileName);
        const remoteFolder = path.dirname(path.relative(appDistDir, fileName));
        const remoteDirs = remoteFolder.split('/');
        let folder = packageRootFolder;
        for(p of remoteDirs){
            if(p == '.') continue;
            folder = (await folder.folders.add(p, true)).folder;            
        }
        logger.verbose(`uploading file ${fileName} (${file.length} bytes)`);
        await folder.files.add(path.basename(fileName), file, true);
    }
    logger.info(`finished deployment of ${packageFolderName}`)
}

main();
